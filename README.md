# CPSC 591 (Rendering) at the University of Calgary Term Project

### By: Elzanne Venter

## Purpose
<div>The goal of my project was to complete three exercises from chapter 28 of Ray Tracing
from The Ground Up by Kevin Suffern. The first task was exercise 28.18 with instructions to
‘render a glass sphere with many small random air bubbles in it’ . The second was exercise
28.27 with instructions to ‘add the glass of water described in Section 28.7 to your ray tracer
and use it to reproduce the images in Figure 28.38. Experiment with different values of
max_depth, filter colors, and dimensions. Compare your images with a real glass of colored
water.’ Finally, the third task was exercise 28.28 with instructions to ‘implement a glass of
water with rounded edges at the bottom of the glass and water.’
The purpose of this project was to gain a better understanding of the techniques used
to render realistic transparency. The benefit of the project was the knowledge and
understanding gained that supplemented my learning in CPSC 591.</div>

## Final Results

### Exercise 28.18
<div> <img src = "./images/2818.png"></img></div>

### Exercise 28.27a
<div> <img src = "./images/2827a.png"></img></div>

### Exercise 28.27b
<div> <img src = "./images/2827b.png"></img></div>

### Exercise 28.28
<div> <img src= "./images/2828.png"></img></div>

## Compiling/Running Instructions (Linux with Eclipse CDT or MacOS with Eclipse CDT)
<ol>
<li>Import wxRayTracer project into eclipse</li>
<li>Change the project settings for your machine (my mac build settings and the linux lab build settings are included as screenshots)</li>
<li>Build the project</li>
<li>Run the application from the Debug directory of the project with the command: ./wxRayTracer</li>
</ol>

## User Interface Notes
<ol>
<li>User must select an exercise before clicking render start. If the user does not, a message box appears to inform the user, and then exits the program. </li>
<li>The colors of the water and glass, and the recursion depth can be modified from the menu bar</li>
</ol>
	
## Resources
<div>This source code was created using the skeleton ray tracer from Ray Tracing From the Ground Up, by Kevin Suffern. In addition, some files were taken from CodeVersion1 and the chapter downloads, also from Ray Tracing from the Ground Up. Some of these 	classes were modified by myself to complete the exercises. Other classes were created from code given in the implementation sections of the textbook. </div> 
	

### Classes (both .cpp and .h files) from CodeVersion1
<ul>
<li>OpenCylinder</li>
<li>ConvexPartSphere</li>
<li>FlatMeshTriangle</li>
<li>MeshTriangle</li>
<li>SmoothMeshTriangle</li>
<li>SmoothTriangle</li>
<li>Triangle</li>
<li>Compound</li>
<li>Geometric Object</li>
<li>Grid</li>
<li>Instance</li>
<li>BBox</li>
<li>Maths</li>
<li>Mesh</li>
<li>ply</li>
<li>PerfectSpecular</li>
<li>Torus</li>
<li>Reflective</li>
<li>Matte</li>
<li>Whitted</li>
</ul>

### Classes (both .cpp and .h files) from Chapter 27 downloads
<ul>
<li>BTDF</li>
<li>Perfect Transmitter</li>
</ul>

### Listings used from Chapter 27 downloads
<ul>
<li>Listing 27.03</li>
</ul>

### Classes (both .cpp and .h files) from Chapter 28 downloads
<ul>
<li>GlassOfWater</li>
</ul>

### Listings used from Chapter 28 downloads
<ul>
<li>Listing 18.1 (FresnelReflector code)</li>
<li>Listing 28.03</li>
</ul>

### Build functions used from Chapter 28 downloads
<ul>
<li>BuildFigure 28.38(a)</li>
<li>BuildFigure 28.38(b)</li>
<li>BuildFigure 28.38(c)</li>
</ul>



### Other 
<ul>
<li>Listing 14.3 for pointlight</li>
<li>Listing 16.1 for phong::shade</li>
<li>Listing 16.2 for in_shadow of pointLight</li>
<li>Listing 19.7 for Disk</li>
<li>Ray Tracing from the Ground Up pg. 379 for Convex and Concave Cylinder. https://blog.csdn.net/libing_zeng/article/details/64131906 for calculations/ implementation of theta and phi and intersections of part tori.</li>
</ul>
	
Resources can be found [here](http://www.raytracegroundup.com/downloads.html) 

## Other Notes
the microfacet material and brdf from my assignment one submission are included in the project, but are not used in any of the scenes
