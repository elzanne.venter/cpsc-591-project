################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../raytracer/Utilities/BBox.cpp \
../raytracer/Utilities/Image.cpp \
../raytracer/Utilities/Maths.cpp \
../raytracer/Utilities/Matrix.cpp \
../raytracer/Utilities/Mesh.cpp \
../raytracer/Utilities/Normal.cpp \
../raytracer/Utilities/Point2D.cpp \
../raytracer/Utilities/Point3D.cpp \
../raytracer/Utilities/RGBColor.cpp \
../raytracer/Utilities/Ray.cpp \
../raytracer/Utilities/ShadeRec.cpp \
../raytracer/Utilities/Vector3D.cpp \
../raytracer/Utilities/ply.cpp 

OBJS += \
./raytracer/Utilities/BBox.o \
./raytracer/Utilities/Image.o \
./raytracer/Utilities/Maths.o \
./raytracer/Utilities/Matrix.o \
./raytracer/Utilities/Mesh.o \
./raytracer/Utilities/Normal.o \
./raytracer/Utilities/Point2D.o \
./raytracer/Utilities/Point3D.o \
./raytracer/Utilities/RGBColor.o \
./raytracer/Utilities/Ray.o \
./raytracer/Utilities/ShadeRec.o \
./raytracer/Utilities/Vector3D.o \
./raytracer/Utilities/ply.o 

CPP_DEPS += \
./raytracer/Utilities/BBox.d \
./raytracer/Utilities/Image.d \
./raytracer/Utilities/Maths.d \
./raytracer/Utilities/Matrix.d \
./raytracer/Utilities/Mesh.d \
./raytracer/Utilities/Normal.d \
./raytracer/Utilities/Point2D.d \
./raytracer/Utilities/Point3D.d \
./raytracer/Utilities/RGBColor.d \
./raytracer/Utilities/Ray.d \
./raytracer/Utilities/ShadeRec.d \
./raytracer/Utilities/Vector3D.d \
./raytracer/Utilities/ply.d 


# Each subdirectory must supply rules for building sources it contributes
raytracer/Utilities/%.o: ../raytracer/Utilities/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D_FILE_OFFSET_BITS=64 -D__WXOSX_COCOA__ -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/build" -I/usr/local/include/wx-3.0 -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/UserInterface" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BRDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BTDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Cameras" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Part Objects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Primitives" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Triangles" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Lights" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Mappings" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Materials" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Noises" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Samplers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Textures" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Tracers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Utilities" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/World" -I/usr/local/lib/wx/include/osx_cocoa-unicode-3.0 -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


