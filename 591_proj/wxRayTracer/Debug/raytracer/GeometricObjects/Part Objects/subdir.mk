################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../raytracer/GeometricObjects/Part\ Objects/ConcavePartCylinder.cpp \
../raytracer/GeometricObjects/Part\ Objects/ConcavePartTorus.cpp \
../raytracer/GeometricObjects/Part\ Objects/ConvexPartCylinder.cpp \
../raytracer/GeometricObjects/Part\ Objects/ConvexPartSphere.cpp \
../raytracer/GeometricObjects/Part\ Objects/ConvexPartTorus.cpp 

OBJS += \
./raytracer/GeometricObjects/Part\ Objects/ConcavePartCylinder.o \
./raytracer/GeometricObjects/Part\ Objects/ConcavePartTorus.o \
./raytracer/GeometricObjects/Part\ Objects/ConvexPartCylinder.o \
./raytracer/GeometricObjects/Part\ Objects/ConvexPartSphere.o \
./raytracer/GeometricObjects/Part\ Objects/ConvexPartTorus.o 

CPP_DEPS += \
./raytracer/GeometricObjects/Part\ Objects/ConcavePartCylinder.d \
./raytracer/GeometricObjects/Part\ Objects/ConcavePartTorus.d \
./raytracer/GeometricObjects/Part\ Objects/ConvexPartCylinder.d \
./raytracer/GeometricObjects/Part\ Objects/ConvexPartSphere.d \
./raytracer/GeometricObjects/Part\ Objects/ConvexPartTorus.d 


# Each subdirectory must supply rules for building sources it contributes
raytracer/GeometricObjects/Part\ Objects/ConcavePartCylinder.o: ../raytracer/GeometricObjects/Part\ Objects/ConcavePartCylinder.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D_FILE_OFFSET_BITS=64 -D__WXOSX_COCOA__ -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/build" -I/usr/local/include/wx-3.0 -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/UserInterface" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BRDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BTDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Cameras" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Part Objects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Primitives" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Triangles" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Lights" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Mappings" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Materials" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Noises" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Samplers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Textures" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Tracers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Utilities" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/World" -I/usr/local/lib/wx/include/osx_cocoa-unicode-3.0 -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"raytracer/GeometricObjects/Part Objects/ConcavePartCylinder.d" -MT"raytracer/GeometricObjects/Part\ Objects/ConcavePartCylinder.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

raytracer/GeometricObjects/Part\ Objects/ConcavePartTorus.o: ../raytracer/GeometricObjects/Part\ Objects/ConcavePartTorus.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D_FILE_OFFSET_BITS=64 -D__WXOSX_COCOA__ -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/build" -I/usr/local/include/wx-3.0 -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/UserInterface" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BRDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BTDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Cameras" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Part Objects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Primitives" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Triangles" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Lights" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Mappings" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Materials" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Noises" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Samplers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Textures" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Tracers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Utilities" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/World" -I/usr/local/lib/wx/include/osx_cocoa-unicode-3.0 -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"raytracer/GeometricObjects/Part Objects/ConcavePartTorus.d" -MT"raytracer/GeometricObjects/Part\ Objects/ConcavePartTorus.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

raytracer/GeometricObjects/Part\ Objects/ConvexPartCylinder.o: ../raytracer/GeometricObjects/Part\ Objects/ConvexPartCylinder.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D_FILE_OFFSET_BITS=64 -D__WXOSX_COCOA__ -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/build" -I/usr/local/include/wx-3.0 -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/UserInterface" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BRDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BTDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Cameras" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Part Objects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Primitives" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Triangles" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Lights" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Mappings" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Materials" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Noises" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Samplers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Textures" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Tracers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Utilities" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/World" -I/usr/local/lib/wx/include/osx_cocoa-unicode-3.0 -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"raytracer/GeometricObjects/Part Objects/ConvexPartCylinder.d" -MT"raytracer/GeometricObjects/Part\ Objects/ConvexPartCylinder.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

raytracer/GeometricObjects/Part\ Objects/ConvexPartSphere.o: ../raytracer/GeometricObjects/Part\ Objects/ConvexPartSphere.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D_FILE_OFFSET_BITS=64 -D__WXOSX_COCOA__ -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/build" -I/usr/local/include/wx-3.0 -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/UserInterface" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BRDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BTDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Cameras" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Part Objects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Primitives" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Triangles" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Lights" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Mappings" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Materials" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Noises" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Samplers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Textures" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Tracers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Utilities" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/World" -I/usr/local/lib/wx/include/osx_cocoa-unicode-3.0 -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"raytracer/GeometricObjects/Part Objects/ConvexPartSphere.d" -MT"raytracer/GeometricObjects/Part\ Objects/ConvexPartSphere.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

raytracer/GeometricObjects/Part\ Objects/ConvexPartTorus.o: ../raytracer/GeometricObjects/Part\ Objects/ConvexPartTorus.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D_FILE_OFFSET_BITS=64 -D__WXOSX_COCOA__ -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/build" -I/usr/local/include/wx-3.0 -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/UserInterface" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BRDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BTDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Cameras" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Part Objects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Primitives" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Triangles" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Lights" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Mappings" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Materials" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Noises" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Samplers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Textures" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Tracers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Utilities" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/World" -I/usr/local/lib/wx/include/osx_cocoa-unicode-3.0 -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"raytracer/GeometricObjects/Part Objects/ConvexPartTorus.d" -MT"raytracer/GeometricObjects/Part\ Objects/ConvexPartTorus.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


