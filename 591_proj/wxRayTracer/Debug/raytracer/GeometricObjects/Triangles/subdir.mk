################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../raytracer/GeometricObjects/Triangles/FlatMeshTriangle.cpp \
../raytracer/GeometricObjects/Triangles/MeshTriangle.cpp \
../raytracer/GeometricObjects/Triangles/SmoothMeshTriangle.cpp \
../raytracer/GeometricObjects/Triangles/SmoothTriangle.cpp \
../raytracer/GeometricObjects/Triangles/Triangle.cpp 

OBJS += \
./raytracer/GeometricObjects/Triangles/FlatMeshTriangle.o \
./raytracer/GeometricObjects/Triangles/MeshTriangle.o \
./raytracer/GeometricObjects/Triangles/SmoothMeshTriangle.o \
./raytracer/GeometricObjects/Triangles/SmoothTriangle.o \
./raytracer/GeometricObjects/Triangles/Triangle.o 

CPP_DEPS += \
./raytracer/GeometricObjects/Triangles/FlatMeshTriangle.d \
./raytracer/GeometricObjects/Triangles/MeshTriangle.d \
./raytracer/GeometricObjects/Triangles/SmoothMeshTriangle.d \
./raytracer/GeometricObjects/Triangles/SmoothTriangle.d \
./raytracer/GeometricObjects/Triangles/Triangle.d 


# Each subdirectory must supply rules for building sources it contributes
raytracer/GeometricObjects/Triangles/%.o: ../raytracer/GeometricObjects/Triangles/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D_FILE_OFFSET_BITS=64 -D__WXOSX_COCOA__ -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/build" -I/usr/local/include/wx-3.0 -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/UserInterface" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BRDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BTDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Cameras" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Part Objects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Primitives" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Triangles" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Lights" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Mappings" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Materials" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Noises" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Samplers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Textures" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Tracers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Utilities" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/World" -I/usr/local/lib/wx/include/osx_cocoa-unicode-3.0 -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


