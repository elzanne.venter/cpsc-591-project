################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../raytracer/GeometricObjects/Primitives/Annulus.cpp \
../raytracer/GeometricObjects/Primitives/ConcaveCylinder.cpp \
../raytracer/GeometricObjects/Primitives/ConvexCylinder.cpp \
../raytracer/GeometricObjects/Primitives/Disk.cpp \
../raytracer/GeometricObjects/Primitives/OpenCylinder.cpp \
../raytracer/GeometricObjects/Primitives/Plane.cpp \
../raytracer/GeometricObjects/Primitives/Sphere.cpp \
../raytracer/GeometricObjects/Primitives/Torus.cpp 

OBJS += \
./raytracer/GeometricObjects/Primitives/Annulus.o \
./raytracer/GeometricObjects/Primitives/ConcaveCylinder.o \
./raytracer/GeometricObjects/Primitives/ConvexCylinder.o \
./raytracer/GeometricObjects/Primitives/Disk.o \
./raytracer/GeometricObjects/Primitives/OpenCylinder.o \
./raytracer/GeometricObjects/Primitives/Plane.o \
./raytracer/GeometricObjects/Primitives/Sphere.o \
./raytracer/GeometricObjects/Primitives/Torus.o 

CPP_DEPS += \
./raytracer/GeometricObjects/Primitives/Annulus.d \
./raytracer/GeometricObjects/Primitives/ConcaveCylinder.d \
./raytracer/GeometricObjects/Primitives/ConvexCylinder.d \
./raytracer/GeometricObjects/Primitives/Disk.d \
./raytracer/GeometricObjects/Primitives/OpenCylinder.d \
./raytracer/GeometricObjects/Primitives/Plane.d \
./raytracer/GeometricObjects/Primitives/Sphere.d \
./raytracer/GeometricObjects/Primitives/Torus.d 


# Each subdirectory must supply rules for building sources it contributes
raytracer/GeometricObjects/Primitives/%.o: ../raytracer/GeometricObjects/Primitives/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D_FILE_OFFSET_BITS=64 -D__WXOSX_COCOA__ -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/build" -I/usr/local/include/wx-3.0 -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/UserInterface" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BRDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/BTDFs" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Cameras" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Part Objects" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Primitives" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/GeometricObjects/Triangles" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Lights" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Mappings" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Materials" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Noises" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Samplers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Textures" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Tracers" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/Utilities" -I"/Users/Elzanne/eclipse-workspace/591_proj/wxRayTracer/raytracer/World" -I/usr/local/lib/wx/include/osx_cocoa-unicode-3.0 -O0 -g3 -Wall -c -fmessage-length=0 -pthread -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


