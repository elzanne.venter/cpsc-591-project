// 	Copyright (C) Kevin Suffern 2000-2007.
//	This C++ code is for non-commercial purposes only.
//	This C++ code is licensed under the GNU General Public License Version 2.
//	See the file COPYING.txt for the full license.


// this implements perfect specular transmission for transparent materials

#include "BTDF.h"

class FresnelTransmitter: public BTDF {
	public:
	
	FresnelTransmitter(void);
		
	FresnelTransmitter(const FresnelTransmitter& pt);
		
		virtual FresnelTransmitter*
		clone(void);
		
		~FresnelTransmitter(void);
		
		FresnelTransmitter&
		operator= (const FresnelTransmitter& rhs);
		
		void
		set_kt(const float k);
		
		void
		set_ior(const float eta);

		void
		set_eta_in(const float k);

		void
		set_eta_out(const float k);

		float
		fresnel(const ShadeRec& sr) const;

		bool													
		tir(const ShadeRec& sr) const;
		
		virtual RGBColor
		f(const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi) const;
		
		virtual RGBColor
		sample_f(const ShadeRec& sr, const Vector3D& wo, Vector3D& wt) const;
		
		virtual RGBColor
		rho(const ShadeRec& sr, const Vector3D& wo) const;
				
	private:
	
		float	kt;			// transmission coefficient
		float	eta_in;
		float	eta_out;
		float	ior;		// index of refraction
};


// -------------------------------------------------------------- set_kt

inline void
FresnelTransmitter::set_kt(const float k) {
	kt = k;
}

// -------------------------------------------------------------- set_ior

inline void
FresnelTransmitter::set_ior(const float eta) {
	ior = eta;
}

inline void
FresnelTransmitter::set_eta_in(const float k){
	eta_in = k;
}

inline void
FresnelTransmitter::set_eta_out(const float k){
	eta_out = k;
}


