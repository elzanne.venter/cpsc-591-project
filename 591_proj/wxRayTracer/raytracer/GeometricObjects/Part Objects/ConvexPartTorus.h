

// 	Copyright (C) Kevin Suffern 2000-2007.
//	This C++ code is for non-commercial purposes only.
//	This C++ code is licensed under the GNU General Public License Version 2.
//	See the file COPYING.txt for the full license.


#include "GeometricObject.h"

class ConvexPartTorus: public GeometricObject {
	public:

		double 		phi_min;			// minimum azimiuth angle in degrees measured counter clockwise from the +ve z axis
		double 		phi_max;			// maximum azimiuth angle in degrees measured counter clockwise from the +ve z axis
		double 		theta_min;			// minimum polar angle in degrees measured down from the +ve y axis
		double 		theta_max;			// maximum polar angle in degrees measured down from the +ve y axis

		double		cos_theta_min;		// stored to avoid repeated calculations
		double		cos_theta_max;		// stored to avoid repeated calculations
		
		ConvexPartTorus(void);
		
		ConvexPartTorus(const double _a, const double _b,const double 	azimuth_min,	// in degrees
				const double 	azimuth_max,	// in degrees
				const double 	polar_min,		// in degrees measured from top
				const double 	polar_max);

		virtual ConvexPartTorus*
		clone(void) const;
	
		ConvexPartTorus(const ConvexPartTorus& torus);
		
		virtual
		~ConvexPartTorus(void);
		
		ConvexPartTorus&
		operator= (ConvexPartTorus& rhs);
		
		Normal 					
		compute_normal(const Point3D& p) const;  												
		
		virtual bool 																 
		hit(const Ray& ray, double& tmin, ShadeRec& sr) const;
		
		virtual bool
		shadow_hit(const Ray& ray, double& tmin) const;

	private:
	
		double 		a;	 	// swept radius 
		double		b;	 	// tube radius
		BBox		bbox;	// the bounding box


};


