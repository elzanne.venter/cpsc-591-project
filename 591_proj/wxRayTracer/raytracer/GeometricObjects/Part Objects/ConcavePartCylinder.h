
// 	Copyright (C) Kevin Suffern 2000-2007.
//	This C++ code is for non-commercial purposes only.
//	This C++ code is licensed under the GNU General Public License Version 2.
//	See the file COPYING.txt for the full license.


// An open cylinder is defined by its extent in the y direction and its radius.
// It is centered on the y axis.
// The classes ConcaveOpenCylinder and ConvexOpenCylinder can inherit from OpenCylinder.
// The classes ConcavePartCylinder and ConvexPartCylinder can in turn inherit from these.


//NOT ACTUALLY IMPLEMENTED AS A PART CYLINDER YET


#include "GeometricObject.h"

class ConcavePartCylinder: public GeometricObject {
	
	public:
		
		ConcavePartCylinder(void);

		ConcavePartCylinder(const double bottom, const double top, const double radius);

		ConcavePartCylinder(const ConcavePartCylinder& c);
		
		virtual ConcavePartCylinder*
		clone (void) const;

		ConcavePartCylinder&
		operator= (const ConcavePartCylinder& rhs);
		
		virtual 												
		~ConcavePartCylinder(void);
						
		virtual bool
		hit(const Ray& ray, double& t, ShadeRec& sr) const;
		
		virtual bool
		shadow_hit(const Ray& ray, double& t) const;

	protected:
	
		double		y0;				// bottom y value
		double		y1;				// top y value
		double		radius;			// radius
		double		inv_radius;  	// one over the radius	
};

