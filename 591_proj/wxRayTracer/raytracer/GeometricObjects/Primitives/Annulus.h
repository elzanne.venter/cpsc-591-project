
// 	Copyright (C) Kevin Suffern 2000-2007.
//	This C++ code is for non-commercial purposes only.
//	This C++ code is licensed under the GNU General Public License Version 2.
//	See the file COPYING.txt for the full license.


// A CutFace is a square in the (x, z) plane with a circular hole in the middle
// A annulus is a circle with a circular hole in the middle

#include "GeometricObject.h"

//-------------------------------------------------------------------------------- class CutFace

class Annulus: public GeometricObject {

	public:
		
		Annulus(void);
		
		Annulus(Point3D c, Normal n, double inner_r, double outer_r);
		
		virtual Annulus*
		clone(void) const;
	
		Annulus(const Annulus& a);

		virtual										
		~Annulus (void);

		Annulus&
		operator= (const Annulus& rhs);
		
		void
		set_center(Point3D c);
		
		void
		set_normal(Normal n);

		void
		set_inner_radius(double inner_r);

		void
		set_outer_radius(double outer_r);
		
		virtual bool 															 
		hit(const Ray& ray, double& tmin, ShadeRec& sr) const;
		
		virtual bool
		shadow_hit(const Ray& ray, double& tmin) const;

	private:

		Point3D	c;
		Normal n;

		double inner_radius;
		double outer_radius;
			
};


// inlined access functions

// ------------------------------------------------------------ set_size

inline void
Annulus::set_center(Point3D p) {
	c = p;
}	

		
// ------------------------------------------------------------ set_radius

inline void
Annulus::set_inner_radius(double inner) {
	inner_radius = inner;
}	

inline void
Annulus::set_outer_radius(double outer) {
	outer_radius = outer;
}

inline void
Annulus::set_normal(Normal normal){
	n = normal;
}

