// 	Copyright (C) Kevin Suffern 2000-2007.
//	This C++ code is for non-commercial purposes only.
//	This C++ code is licensed under the GNU General Public License Version 2.
//	See the file COPYING.txt for the full license.

#include "Disk.h"

const double Disk::kEpsilon = 0.001;

// ----------------------------------------------------------------------  default constructor

Disk::Disk(void)
	: 	GeometricObject(),
		c(0.0),
		n(0, 1, 0),
		r(1)
{}


// ----------------------------------------------------------------------  constructor

Disk::Disk(const Point3D& point, const Normal& normal, float radius)
	:	GeometricObject(),
		c(point),
		n(normal),
		r(radius)
{
		n.normalize();
}


// ---------------------------------------------------------------- copy constructor

Disk::Disk(const Disk& disk)
	:	GeometricObject(disk),
		c(disk.c),
		n(disk.n),
		r(disk.r)
{}


// ---------------------------------------------------------------- clone

Disk*
Disk::clone(void) const {
	return (new Disk(*this));
}


// ---------------------------------------------------------------- assignment operator

Disk&
Disk::operator= (const Disk& rhs)	{
	
	if (this == &rhs)
		return (*this);

	GeometricObject::operator= (rhs);

	c = rhs.c;
	n = rhs.n;
	r = rhs.r;

	return (*this);
}


// ---------------------------------------------------------------- destructor

Disk::~Disk(void)
{}


// ----------------------------------------------------------------- hit

bool 															 
Disk::hit(const Ray& ray, double& tmin, ShadeRec& sr) const {

	double t = (c - ray.o) * n / (ray.d * n);

	if (t <= kEpsilon)
		return (false);

	Point3D p = ray.o + t * ray.d;
		
	if (c.d_squared(p) < (r * r)) {
		tmin 				= t;
		sr.normal 			= n;
		sr.local_hit_point	= p;
		return (true);	
	}
	else
		return (false);
}

bool
Disk::shadow_hit(const Ray& ray, double& tmin) const {

	double t = (c - ray.o) * n / (ray.d * n);

	if (t <= kEpsilon)
		return (false);

	Point3D p = ray.o + t * ray.d;

	if (c.d_squared(p) < (r * r)) {
		tmin 				= t;
		return (true);
	}
	else
		return (false);
}

