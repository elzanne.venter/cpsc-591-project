// 	Copyright (C) Kevin Suffern 2000-2007.
//	This C++ code is for non-commercial purposes only.
//	This C++ code is licensed under the GNU General Public License Version 2.
//	See the file COPYING.txt for the full license.


#include <Annulus.h>

// ----------------------------------------------------------------  default constructor

Annulus::Annulus(void)
	:	c(Point3D(0,0,0)),
	 	n(Normal(0,1,0)),
		inner_radius(0.5),
		outer_radius(1.0)
{}


// ----------------------------------------------------------------  constructor


Annulus::Annulus(Point3D center, Normal norm, double inner, double outer)
	: 	c(center),
	 	n(norm),
		inner_radius(inner),
		outer_radius(outer)
{}




// ---------------------------------------------------------------- clone

Annulus*
Annulus::clone(void) const {
	return(new Annulus (*this));
}


// ---------------------------------------------------------------- copy constructor

Annulus::Annulus(const Annulus& a)
	:	c(a.c),
	 	n(a.n),
		inner_radius(a.inner_radius),
		outer_radius(a.outer_radius)
{}



// ---------------------------------------------------------------- assignment operator

Annulus&
Annulus::operator= (const Annulus& rhs) {
	if (this == &rhs)
		return (*this);
	
	c = rhs.c;
	n = rhs.n;
	inner_radius = rhs.inner_radius;
	outer_radius = rhs.outer_radius;

	return (*this);
}


// ---------------------------------------------------------------- destructor

Annulus::~Annulus(void) {}



// ------------------------------------------------------------------------------ hit

bool 															 
Annulus::hit(const Ray& ray, double& tmin, ShadeRec& sr) const {

	double t = (c - ray.o) * n / (ray.d * n);

	if (t <= kEpsilon)
		return (false);

	Point3D p = ray.o + t * ray.d;


	//****I THNK THIS WILL WORK. COULD BE A BUG THOUGH
	if ((c.d_squared(p) < (outer_radius * outer_radius)) && (c.d_squared(p) > (inner_radius * inner_radius))) {
		tmin 				= t;
		sr.normal 			= n;
		sr.local_hit_point	= p;
		return (true);
	}
	else
		return (false);
}

bool
Annulus::shadow_hit(const Ray& ray, double& tmin) const {

	double t = (c - ray.o) * n / (ray.d * n);

	if (t <= kEpsilon)
		return (false);

	Point3D p = ray.o + t * ray.d;


	//POTENTIAL SAME BUG AS IN LAST FUNCTION^^^
	if ((c.d_squared(p) < (outer_radius * outer_radius)) && (c.d_squared(p) > (inner_radius * inner_radius))) {
		tmin 				= t;
		return (true);
	}
	else
		return (false);
}






