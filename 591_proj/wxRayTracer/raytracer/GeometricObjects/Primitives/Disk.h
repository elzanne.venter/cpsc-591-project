
// 	Copyright (C) Kevin Suffern 2000-2007.
//	This C++ code is for non-commercial purposes only.
//	This C++ code is licensed under the GNU General Public License Version 2.
//	See the file COPYING.txt for the full license.


#include "GeometricObject.h"

//-------------------------------------------------------------------- class Plane

class Disk: public GeometricObject {
	
	public:
	
		Disk(void);   												// default constructor
		
		Disk(const Point3D& point, const Normal& normal, float radius);			// constructor
	
		Disk(const Disk& disk); 									// copy constructor
		
		virtual Disk* 												// virtual copy constructor
		clone(void) const;

		Disk& 														// assignment operator
		operator= (const Disk& rhs);
		
		virtual														// destructor
		~Disk(void);
					
		virtual bool 																								 
		hit(const Ray& ray, double& tmin, ShadeRec& sr) const;
		
		virtual bool
		shadow_hit(const Ray&ray, double& tmin) const;

	private:
	
		Point3D 	c;   				// point through which plane passes
		Normal 		n;					// normal to the plane
		float 		r;
				
		static const double kEpsilon;   // for shadows and secondary rays
};
