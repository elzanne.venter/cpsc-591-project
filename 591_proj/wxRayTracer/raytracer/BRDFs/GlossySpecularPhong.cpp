#include "GlossySpecularPhong.h"
#include "Constants.h"

// ---------------------------------------------------------------------- default constructor

GlossySpecularPhong::GlossySpecularPhong(void)
	:   BRDF(),
		cs(0.0),
		exp(1),
		ks(0.25)
{}


// ---------------------------------------------------------------------- copy constructor

GlossySpecularPhong::GlossySpecularPhong(const GlossySpecularPhong& glos)
	:   BRDF(glos),
		cs(glos.cs),
		exp(1),
		ks(0.25)
{}


// ---------------------------------------------------------------------- clone

GlossySpecularPhong*
GlossySpecularPhong::clone(void) const {
	return (new GlossySpecularPhong(*this));
}	


// ---------------------------------------------------------------------- destructor

GlossySpecularPhong::~GlossySpecularPhong(void) {}


// ---------------------------------------------------------------------- assignment operator

GlossySpecularPhong&
GlossySpecularPhong::operator= (const GlossySpecularPhong& rhs) {
	if (this == &rhs)
		return (*this);
		
	BRDF::operator= (rhs);
	
	cs = rhs.cs;
	
	return (*this);
}


// ---------------------------------------------------------------------- f

RGBColor
GlossySpecularPhong::f(const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi) const {


	Vector3D reflection = (-1 * wi) + (2.0 * sr.normal) * (sr.normal * wi);
	float dot = reflection * wo;
	RGBColor output = ks * pow(dot,exp) * cs;

	return output;
}


// ---------------------------------------------------------------------- rho

RGBColor
GlossySpecularPhong::rho(const ShadeRec& sr, const Vector3D& wo) const {
	return (black);
}


