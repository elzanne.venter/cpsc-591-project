#include "GlossySpecular.h"
#include "Constants.h"

// ---------------------------------------------------------------------- default constructor

GlossySpecular::GlossySpecular(void)
	:   BRDF(),
		cs(0.0),
		r(0.5) //SET ROUGHNESS HERE
{}


// ---------------------------------------------------------------------- copy constructor

GlossySpecular::GlossySpecular(const GlossySpecular& glos)
	:   BRDF(glos),
		cs(glos.cs),
		r(0.5) //SET ROUGHNESS HERE
{}


// ---------------------------------------------------------------------- clone

GlossySpecular*
GlossySpecular::clone(void) const {
	return (new GlossySpecular(*this));
}	


// ---------------------------------------------------------------------- destructor

GlossySpecular::~GlossySpecular(void) {}


// ---------------------------------------------------------------------- assignment operator

GlossySpecular&
GlossySpecular::operator= (const GlossySpecular& rhs) {
	if (this == &rhs)
		return (*this);
		
	BRDF::operator= (rhs);
	
	cs = rhs.cs;
	r = rhs.r;
	
	return (*this);
}


// ---------------------------------------------------------------------- f

RGBColor
GlossySpecular::f(const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi) const {

	Vector3D wh = wi + wo;
	Normal normal = sr.normal;
	wh.normalize();

	//GEOMETRIC
	float k = pow(r+1,2)/8;
	float g1 = 1/((wh * wi)* (1-k)+k);
	float g2 = 1/((wh * wo)* (1-k)+k);
	float geometric_G = g1*g2;

	//MICROFACET DISTRUBUTION

	//BECKMANN
	float b_alpha = acos(normal * wh);
	float b_numerator = pow(E, -1 * pow((tan(b_alpha)/r),2));
	float b_denominator = pow(r,2) * pow(cos(b_alpha),4);
	float beckmann_D = b_numerator/b_denominator;

	return beckmann_D;

	//GGX
	float ggx_alpha = pow(r,2);
	float ggx_numerator = pow(ggx_alpha,2);
	float ggx_denominator = PI * pow(pow(normal * wh,2) * (pow(ggx_alpha,2)-1) + 1,2);
	float ggx_D = ggx_numerator / ggx_denominator;

	float fres_co = pow(1-(wi * wh),5);

	RGBColor fres_F = cs + ((white + (-1 * cs)) * fres_co);


	RGBColor final_numerator_Beck = fres_F * geometric_G * beckmann_D;

	RGBColor final_numerator_GGX = fres_F * geometric_G * ggx_D;

	float final_denominator = 4 * (normal * wi) * (normal * wo);

	RGBColor final_ggx = final_numerator_GGX / final_denominator;

	RGBColor final_beck = final_numerator_Beck / final_denominator;




	//TOGGLE BETWEEN GGX AND BECKMAN DISTRIBUTION FUNCTIONS BY CHANGING swapping final_beck and final_ggx
	//return final_beck;
	return final_ggx;
}


// ---------------------------------------------------------------------- rho

RGBColor
GlossySpecular::rho(const ShadeRec& sr, const Vector3D& wo) const {
	return (black);
}


