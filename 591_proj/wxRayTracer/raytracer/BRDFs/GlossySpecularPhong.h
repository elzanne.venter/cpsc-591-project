#include "BRDF.h"

class GlossySpecularPhong: public BRDF {
	public:
	
	GlossySpecularPhong(void);
		
	GlossySpecularPhong(const GlossySpecularPhong& glos);
		
		virtual GlossySpecularPhong*
		clone(void) const;
		
		~GlossySpecularPhong(void);
		
		GlossySpecularPhong&
		operator= (const GlossySpecularPhong& rhs);
		
		virtual RGBColor
		f(const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi) const;
		
		virtual RGBColor
		rho(const ShadeRec& sr, const Vector3D& wo) const;
				
		
		void
		set_cs(const RGBColor& c);
		
		void													
		set_cs(const float r, const float g, const float b);
		
		void													
		set_cs(const float c);

		void
		set_ks(float k);

		void
		set_exp(int exp);
					
	private:
	
		float		ks;
		RGBColor 	cs;
		int			exp;

};




// -------------------------------------------------------------- set_ka



// -------------------------------------------------------------- set_kd



// -------------------------------------------------------------- set_cd

inline void
GlossySpecularPhong::set_cs(const RGBColor& c) {
	cs = c;
}


// ---------------------------------------------------------------- set_cd

inline void													
GlossySpecularPhong::set_cs(const float r, const float g, const float b) {
	cs.r = r; cs.g = g; cs.b = b;
}


// ---------------------------------------------------------------- set_cd

inline void													
GlossySpecularPhong::set_cs(const float c) {
	cs.r = c; cs.g = c; cs.b = c;
}

inline void
GlossySpecularPhong::set_ks(float k){
	ks = k;
}

inline void
GlossySpecularPhong::set_exp(int x)
{
	exp = x;
}

