#ifndef __GLOSSYSPECULAR__
#define __GLOSSYSPECULAR__

#include "BRDF.h"

class GlossySpecular: public BRDF {
	public:
	
	GlossySpecular(void);
		
	GlossySpecular(const GlossySpecular& glos);
		
		virtual GlossySpecular*
		clone(void) const;
		
		~GlossySpecular(void);
		
		GlossySpecular&
		operator= (const GlossySpecular& rhs);
		
		virtual RGBColor
		f(const ShadeRec& sr, const Vector3D& wo, const Vector3D& wi) const;
		
		virtual RGBColor
		rho(const ShadeRec& sr, const Vector3D& wo) const;
				
		
		void
		set_cs(const RGBColor& c);
		
		void													
		set_cs(const float r, const float g, const float b);
		
		void													
		set_cs(const float c);
					
	private:
	
		RGBColor 	cs;
		float r;
};




// -------------------------------------------------------------- set_ka



// -------------------------------------------------------------- set_kd



// -------------------------------------------------------------- set_cd

inline void
GlossySpecular::set_cs(const RGBColor& c) {
	cs = c;
}


// ---------------------------------------------------------------- set_cd

inline void													
GlossySpecular::set_cs(const float r, const float g, const float b) {
	cs.r = r; cs.g = g; cs.b = b;
}


// ---------------------------------------------------------------- set_cd

inline void													
GlossySpecular::set_cs(const float c) {
	cs.r = c; cs.g = c; cs.b = c;
}

#endif
