#include "FresnelTransmitter.h"
#include "FresnelReflector.h"
#import "Phong.h"

class Dielectric: public Phong{

public:

	Dielectric(void);

	Dielectric(const Dielectric& m);

	virtual Material*
	clone(void) const;

	Dielectric&
	operator= (const Dielectric& rhs);

	~Dielectric(void);

	void
	set_eta_in(const float k);

	void
	set_eta_out(const float k);

	void
	set_ior(const float c);

	void
	set_cf_in(RGBColor c);

	void
	set_cf_out(RGBColor c);

	virtual RGBColor
	shade(ShadeRec& sr);

private:
	FresnelReflector* fresnel_brdf;
	FresnelTransmitter* fresnel_btdf;

	RGBColor 			cf_in;
	RGBColor			cf_out;
};

// ---------------------------------------------------------------- set_kr

inline void
Dielectric::set_ior(const float k) {
	fresnel_btdf->set_ior(k);
}
// ---------------------------------------------------------------- set_cr

inline void
Dielectric::set_eta_in(const float c) {
	fresnel_brdf->set_eta_in(c);
	fresnel_btdf->set_eta_in(c);
}

inline void
Dielectric::set_eta_out(const float c){
	fresnel_brdf->set_eta_out(c);
	fresnel_btdf->set_eta_out(c);
}

inline void
Dielectric::set_cf_in(RGBColor c){
	cf_in = c;
}

inline void
Dielectric::set_cf_out(RGBColor c){
	cf_out= c;
}

