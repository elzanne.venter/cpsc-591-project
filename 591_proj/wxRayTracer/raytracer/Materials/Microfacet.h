#ifndef __MICROFACET__
#define __MICROFACET__

#include "Material.h"
#include "Lambertian.h"
#include "GlossySpecular.h"

//----------------------------------------------------------------------------- class TorGloss

class Microfacet: public Material {
	public:
			
	Microfacet(void);

	Microfacet(const Microfacet& g);
		
		virtual Material*										
		clone(void) const;									

		Microfacet&
		operator= (const Microfacet& rhs);

		~Microfacet(void);
		
		void 													
		set_ka(const float k);
		
		void 													
		set_kd(const float k);
		
		void													
		set_cd(const RGBColor c);
		
		void													
		set_cd(const float r, const float g, const float b);
		
		void																						
		set_cd(const float c);


		void set_cs(const RGBColor c);

		void set_cs(const float r, const float g, const float b);

		void set_cs(const float c);
				
		virtual RGBColor										
		shade(ShadeRec& sr);
		
	private:
		
		Lambertian*		ambient_brdf;
		Lambertian*		diffuse_brdf;
		GlossySpecular*	glossy_brdf;
};


// ---------------------------------------------------------------- set_ka
// this sets Lambertian::kd
// there is no Lambertian::ka data member because ambient reflection 
// is diffuse reflection

inline void								
Microfacet::set_ka(const float ka) {
	ambient_brdf->set_kd(ka);
}


// ---------------------------------------------------------------- set_kd
// this also sets Lambertian::kd, but for a different Lambertian object

inline void								
Microfacet::set_kd (const float kd) {
	diffuse_brdf->set_kd(kd);
}


// ---------------------------------------------------------------- set_cd

inline void												
Microfacet::set_cd(const RGBColor c) {
	ambient_brdf->set_cd(c);
	diffuse_brdf->set_cd(c);
}


// ---------------------------------------------------------------- set_cd

inline void													
Microfacet::set_cd(const float r, const float g, const float b) {
	ambient_brdf->set_cd(r, g, b);
	diffuse_brdf->set_cd(r, g, b);
}

// ---------------------------------------------------------------- set_cd

inline void													
Microfacet::set_cd(const float c) {
	ambient_brdf->set_cd(c);
	diffuse_brdf->set_cd(c);
}

inline void
Microfacet::set_cs(const RGBColor c){
	ambient_brdf->set_cd(c);
	diffuse_brdf->set_cd(c);
	glossy_brdf->set_cs(c);
}

inline void
Microfacet::set_cs(const float r, const float g, const float b){
	ambient_brdf->set_cd(r,g,b);
	diffuse_brdf->set_cd(r,g,b);
	glossy_brdf->set_cs(r,g,b);
}

inline void
Microfacet::set_cs(const float c){
	ambient_brdf->set_cd(c);
	diffuse_brdf->set_cd(c);
	glossy_brdf->set_cs(c);
}

#endif
