#include "Microfacet.h"

// ---------------------------------------------------------------- default constructor

Microfacet::Microfacet (void)
	:	Material(),
		ambient_brdf(new Lambertian),
		diffuse_brdf(new Lambertian),
		glossy_brdf(new GlossySpecular)
{}



// ---------------------------------------------------------------- copy constructor

Microfacet::Microfacet(const Microfacet& g)
	: 	Material(g)
{
	if(g.ambient_brdf)
		ambient_brdf = g.ambient_brdf->clone();
	else  ambient_brdf = NULL;
	
	if(g.diffuse_brdf)
		diffuse_brdf = g.diffuse_brdf->clone();
	else  diffuse_brdf = NULL;

	if(g.glossy_brdf)
		glossy_brdf = g.glossy_brdf->clone();

}


// ---------------------------------------------------------------- clone

Material*										
Microfacet::clone(void) const {
	return (new Microfacet(*this));
}	


// ---------------------------------------------------------------- assignment operator

Microfacet&
Microfacet::operator= (const Microfacet& rhs) {
	if (this == &rhs)
		return (*this);
		
	Material::operator=(rhs);
	
	if (ambient_brdf) {
		delete ambient_brdf;
		ambient_brdf = NULL;
	}

	if (rhs.ambient_brdf)
		ambient_brdf = rhs.ambient_brdf->clone();
		
	if (diffuse_brdf) {
		delete diffuse_brdf;
		diffuse_brdf = NULL;
	}

	if (rhs.diffuse_brdf)
		diffuse_brdf = rhs.diffuse_brdf->clone();

	if (glossy_brdf){
		delete glossy_brdf;
		glossy_brdf = NULL;
	}

	if (rhs.glossy_brdf){
		glossy_brdf = rhs.glossy_brdf->clone();
	}


	return (*this);
}


// ---------------------------------------------------------------- destructor

Microfacet::~Microfacet(void) {

	if (ambient_brdf) {
		delete ambient_brdf;
		ambient_brdf = NULL;
	}
	
	if (diffuse_brdf) {
		delete diffuse_brdf;
		diffuse_brdf = NULL;
	}

	if (glossy_brdf){
		delete glossy_brdf;
		glossy_brdf = NULL;
	}

}


// ---------------------------------------------------------------- shade

RGBColor
Microfacet::shade(ShadeRec& sr) {
	Vector3D 	wo 			= -sr.ray.d;
	RGBColor 	L 			= ambient_brdf->rho(sr, wo) * sr.w.ambient_ptr->L(sr);
	int 		num_lights	= sr.w.lights.size();
	
	for (int j = 0; j < num_lights; j++) {
		Vector3D wi = sr.w.lights[j]->get_direction(sr);    
		float ndotwi = sr.normal * wi;
	
		if (ndotwi > 0.0) 
			L += diffuse_brdf->f(sr, wo, wi) + glossy_brdf->f(sr,wo,wi) * sr.w.lights[j]->L(sr) * ndotwi;
	}
	
	return (L);
}



