#include "PerfectTransmitter.h"
#include "PerfectSpecular.h"
#import "Phong.h"

class Transparent: public Phong{

public:

	Transparent(void);

	Transparent(const Transparent& m);

	virtual Material*
	clone(void) const;

	Transparent&
	operator= (const Transparent& rhs);

	~Transparent(void);

	void
	set_kt(const float k);

	void
	set_kr(const float k);

	void
	set_ior(const float c);

	virtual RGBColor
	shade(ShadeRec& sr);

private:
	PerfectSpecular* reflective_brdf;
	PerfectTransmitter* specular_btdf;
};

// ---------------------------------------------------------------- set_kr

inline void
Transparent::set_kt(const float k) {
	specular_btdf->set_kt(k);
}


// ---------------------------------------------------------------- set_cr

inline void
Transparent::set_kr(const float c) {
	reflective_brdf->set_kr(c);
}

inline void
Transparent::set_ior(const float c){
	specular_btdf->set_ior(c);
}

