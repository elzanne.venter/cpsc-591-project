// This builds a scene that consists of 35 shaded spheres and a plane.
// The objects are illuminated by a directional light and rendered with
// ambient and diffuse shading.
// Perspective viewing is used with a pinhole camera.
// Jittered sampling for antialiasing is hardwired into the PinHole::render_scene function.
// There are no sampler classes in this project.
// These are in the Chapter 5 download file.
// The spheres are the same as those in the Chapter 14 page one image. 

#import "Mesh.h"
#import "Grid.h"
#include <unistd.h>


void 												
World::build2818(int depth) {
	int num_samples = 1; 
	
	// view plane
	vp.set_hres(600);
	vp.set_vres(600);
	vp.set_pixel_size(0.5);
	vp.set_samples(num_samples);
	vp.set_maxdepth(depth);
	
	// the ambient light here is the same as the default set in the World
	// constructor, and can therefore be left out
	
	Ambient* ambient_ptr = new Ambient;
	ambient_ptr->scale_radiance(0.2);
	set_ambient_light(ambient_ptr); 

	background_color = RGBColor(black);			// default color - this can be left out
	
	tracer_ptr = new Whitted(this);

	
	// camera
	
	Pinhole* pinhole_ptr = new Pinhole;
	pinhole_ptr->set_eye(0.0,0.0,500.0); //0.0,0.0,500.0 -sphere -0.45,0.15,0.55 -bunny
	pinhole_ptr->set_lookat(0.0,0.0,0.0); //0.0,0.0,0.0 -sphere 0.0,0.1,0.0 -bunny
	pinhole_ptr->set_view_distance(600.0);
	pinhole_ptr->compute_uvw();     
	set_camera(pinhole_ptr);


	PointLight* light_ptr2 = new PointLight;
	light_ptr2->set_location(Vector3D(0,100,40));
	light_ptr2->scale_radiance(1.0);
	light_ptr2->set_shadows(false);
	add_light(light_ptr2);

	
	// colors

	RGBColor yellow(1, 1, 0);										// yellow
	RGBColor brown(0.71, 0.40, 0.16);								// brown
	RGBColor darkGreen(0.0, 0.41, 0.41);							// darkGreen
	RGBColor orange(1, 0.75, 0);									// orange
	RGBColor green(0, 0.6, 0.3);									// green
	RGBColor lightGreen(0.65, 1, 0.30);								// light green
	RGBColor darkYellow(0.61, 0.61, 0);								// dark yellow
	RGBColor lightPurple(0.65, 0.3, 1);								// light purple
	RGBColor darkPurple(0.5, 0, 1);									// dark purple
	RGBColor grey(0.25);// grey
	RGBColor skyBlue(0.6,0.8,0.92);
	
	RGBColor lightBrown(0.85,0.74,0.61);

	
	// Ambient coefficient
	
	float ka = 0.4;
	float kd = 0.25;
	float ks = 0.75;
	
	

	// spheres


	Matte* matte_ptr1 = new Matte;
	matte_ptr1->set_ka(ka);
	matte_ptr1->set_kd(kd);
	matte_ptr1->set_cd(red);

	Phong* phong_ptr1 = new Phong;
	phong_ptr1->set_ka(ka);
	phong_ptr1->set_kd(kd);
	phong_ptr1->set_ks(ks);
	phong_ptr1->set_exp(30);
	phong_ptr1->set_cd(darkPurple);
	phong_ptr1->set_cs(darkPurple);

	Reflective* reflective_ptr1 = new Reflective;
	reflective_ptr1->set_ka(1);
	reflective_ptr1->set_kd(0.1);
	reflective_ptr1->set_cd(lightBrown);
	reflective_ptr1->set_ks(0.9);
	reflective_ptr1->set_exp(100);
	reflective_ptr1->set_kr(0.75);
	reflective_ptr1->set_cr(white);

	Reflective*	reflective_ptr2 = new Reflective;
	reflective_ptr2->set_ka(0.3);
	reflective_ptr2->set_kd(0.3);
	reflective_ptr2->set_cd(red);
	reflective_ptr2->set_ks(0.2);
	reflective_ptr2->set_exp(2000.0);
	reflective_ptr2->set_kr(0.25);

	Transparent* glass_ptr = new Transparent;
	glass_ptr->set_ks(0.5);
	glass_ptr->set_exp(2000);
	glass_ptr->set_ior(1.5);
	glass_ptr->set_kr(0.1);
	glass_ptr->set_kt(0.9);

	Dielectric* dielectric_ptr1 = new Dielectric;
	dielectric_ptr1->set_ks(0.2);
	dielectric_ptr1->set_exp(2000);
	dielectric_ptr1->set_cf_in(white);
	dielectric_ptr1->set_cf_out(white);
	dielectric_ptr1->set_eta_in(1.5);
	dielectric_ptr1->set_eta_out(1.0);

	Dielectric* dielectric_ptr2 = new Dielectric;
	dielectric_ptr2->set_ks(0.2);
	dielectric_ptr2->set_exp(2000);
	dielectric_ptr2->set_cf_in(white);
	dielectric_ptr2->set_cf_out(white);
	dielectric_ptr2->set_eta_in(1.0);
	dielectric_ptr2->set_eta_out(1.5);

	Dielectric* dielectric_ptr3 = new Dielectric;
	dielectric_ptr3->set_ks(0.2);
	dielectric_ptr3->set_exp(2000);
	dielectric_ptr3->set_cf_in(white);
	dielectric_ptr3->set_cf_out(white);
	dielectric_ptr3->set_eta_in(1.0);
	dielectric_ptr3->set_eta_out(1.5);

	Dielectric* dielectric_ptr4 = new Dielectric;
	dielectric_ptr4->set_ks(0.2);
	dielectric_ptr4->set_exp(2000);
	dielectric_ptr4->set_cf_in(white);
	dielectric_ptr4->set_cf_out(white);
	dielectric_ptr4->set_eta_in(1.0);
	dielectric_ptr4->set_eta_out(1.5);

	Dielectric* dielectric_ptr5 = new Dielectric;
	dielectric_ptr5->set_ks(0.2);
	dielectric_ptr5->set_exp(2000);
	dielectric_ptr5->set_cf_in(white);
	dielectric_ptr5->set_cf_out(white);
	dielectric_ptr5->set_eta_in(1.0);
	dielectric_ptr5->set_eta_out(1.5);

	Dielectric* dielectric_ptr6 = new Dielectric;
	dielectric_ptr6->set_ks(0.2);
	dielectric_ptr6->set_exp(2000);
	dielectric_ptr6->set_cf_in(white);
	dielectric_ptr6->set_cf_out(white);
	dielectric_ptr6->set_eta_in(1.0);
	dielectric_ptr6->set_eta_out(1.5);

	Dielectric* dielectric_ptr7 = new Dielectric;
	dielectric_ptr7->set_ks(0.2);
	dielectric_ptr7->set_exp(2000);
	dielectric_ptr7->set_cf_in(white);
	dielectric_ptr7->set_cf_out(white);
	dielectric_ptr7->set_eta_in(1.0);
	dielectric_ptr7->set_eta_out(1.5);

	Dielectric* dielectric_ptr8 = new Dielectric;
	dielectric_ptr8->set_ks(0.2);
	dielectric_ptr8->set_exp(2000);
	dielectric_ptr8->set_cf_in(white);
	dielectric_ptr8->set_cf_out(white);
	dielectric_ptr8->set_eta_in(1.0);
	dielectric_ptr8->set_eta_out(1.5);

	Dielectric* dielectric_ptr9 = new Dielectric;
	dielectric_ptr9->set_ks(0.2);
	dielectric_ptr9->set_exp(2000);
	dielectric_ptr9->set_cf_in(white);
	dielectric_ptr9->set_cf_out(white);
	dielectric_ptr9->set_eta_in(1.0);
	dielectric_ptr9->set_eta_out(1.5);

	Dielectric* dielectric_ptr10 = new Dielectric;
	dielectric_ptr10->set_ks(0.2);
	dielectric_ptr10->set_exp(2000);
	dielectric_ptr10->set_cf_in(white);
	dielectric_ptr10->set_cf_out(white);
	dielectric_ptr10->set_eta_in(1.0);
	dielectric_ptr10->set_eta_out(1.5);




	Sphere*	sphere_ptr1 = new Sphere(Point3D(10, 30, 40), 50);
	sphere_ptr1->set_material(dielectric_ptr1);
	add_object(sphere_ptr1);

	Sphere*	sphere_ptr2 = new Sphere(Point3D(10, 30, 20), 3);
	sphere_ptr2->set_material(dielectric_ptr2);
	add_object(sphere_ptr2);

	Sphere*	sphere_ptr3 = new Sphere(Point3D(13, 30, 20), 1);
	sphere_ptr3->set_material(dielectric_ptr3);
	add_object(sphere_ptr3);

	Sphere*	sphere_ptr4 = new Sphere(Point3D(13, 33, 20), 3.2);
	sphere_ptr4	->set_material(dielectric_ptr4);
	add_object(sphere_ptr4);

	Sphere*	sphere_ptr5 = new Sphere(Point3D(-9, 33, 10), 1.5);
	sphere_ptr5	->set_material(dielectric_ptr5);
	add_object(sphere_ptr5);

	Sphere*	sphere_ptr6 = new Sphere(Point3D(5, 15, 10), 3);
	sphere_ptr6	->set_material(dielectric_ptr6);
	add_object(sphere_ptr6);

	Sphere*	sphere_ptr7 = new Sphere(Point3D(20, 15, 10), 2);
	sphere_ptr7	->set_material(dielectric_ptr7);
	add_object(sphere_ptr7);

	Sphere*	sphere_ptr8 = new Sphere(Point3D(17, 45, 20), 4.5);
	sphere_ptr8	->set_material(dielectric_ptr8);
	add_object(sphere_ptr8);

	Sphere*	sphere_ptr9 = new Sphere(Point3D(-2.5, 45, 15), 5.5);
	sphere_ptr9	->set_material(dielectric_ptr9);
	add_object(sphere_ptr9);

	Sphere*	sphere_ptr10 = new Sphere(Point3D(19, 45, 20), 2.1);
	sphere_ptr10	->set_material(dielectric_ptr10);
	add_object(sphere_ptr10);


	Plane* plane_ptr1 = new Plane (Point3D(0,-30,0),Normal(0,1,0));
	plane_ptr1->set_material(reflective_ptr1);
	add_object(plane_ptr1);


	Plane* plane_ptr2 = new Plane (Point3D(10,0,0),Normal(0,0,1));
	plane_ptr2->set_material(matte_ptr1);
	add_object(plane_ptr2);


}


void
World::build2827(int depth) {

	int num_samples = 1;

	// view plane
	vp.set_hres(600);
	vp.set_vres(600);
	vp.set_samples(num_samples);
	vp.set_maxdepth(depth);


	background_color = RGBColor(0.75);

	tracer_ptr = new Whitted(this);

	Ambient* ambient_ptr = new Ambient;
	ambient_ptr->scale_radiance(0.5);
	set_ambient_light(ambient_ptr);


	Pinhole* pinhole_ptr = new Pinhole;
	pinhole_ptr->set_eye(5, 6, 10);
	pinhole_ptr->set_lookat(0, 1, 0);
	pinhole_ptr->set_view_distance(1250.0);
	pinhole_ptr->compute_uvw();
	set_camera(pinhole_ptr);


	PointLight* light_ptr1 = new PointLight;
	light_ptr1->set_location(Vector3D(40, 50, 30));
	light_ptr1->scale_radiance(3.0);
	light_ptr1->set_shadows(true);
	add_light(light_ptr1);


	// materials for the glass of water

	// glass-air boundary

	RGBColor glass_color(0.65, 1, 0.75);
	RGBColor water_color(1, 0.25, 1);

	Dielectric* glass_ptr = new Dielectric;
	glass_ptr->set_eta_in(1.50);			// glass
	glass_ptr->set_eta_out(1.0);			// air
	glass_ptr->set_cf_in(glass_color);
	glass_ptr->set_cf_out(white);

	// water-air boundary

	Dielectric* water_ptr = new Dielectric;
	water_ptr->set_eta_in(1.33);			// water
	water_ptr->set_eta_out(1.0);			// air
	water_ptr->set_cf_in(water_color);
	water_ptr->set_cf_out(white);

	// water-glass boundary

	Dielectric* dielectric_ptr = new Dielectric;
	dielectric_ptr->set_eta_in(1.33); 		// water
	dielectric_ptr->set_eta_out(1.50); 		// glass
	dielectric_ptr->set_cf_in(water_color);
	dielectric_ptr->set_cf_out(glass_color);

	Matte* matte_ptr_2 = new Matte;
	matte_ptr_2->set_ka(0.25);
	matte_ptr_2->set_kd(0.65);
	matte_ptr_2->set_cd(0.5);

	Disk* test = new Disk(Point3D(0), Normal(0, -1, 0),0.9 + 0.1);
	test->set_material(matte_ptr_2);
	add_object(test);


	// Define the GlassOfWater object
	// The parameters below are the default values, but using the constructor that
	// takes these as arguments makes it easier to experiment with different values
	/*
	double height 			= 2.0;
	double inner_radius 	= 0.9;
	double wall_thickness 	= 0.1;
	double base_thickness 	= 0.3;
	double water_height 	= 1.5;
	double meniscus_radius 	= 0.1;

	GlassOfWater* glass_of_water_ptr = new GlassOfWater(height,
														inner_radius,
														wall_thickness,
														base_thickness,
														water_height,
														meniscus_radius);

	glass_of_water_ptr->set_glass_air_material(glass_ptr);
	glass_of_water_ptr->set_water_air_material(water_ptr);
	glass_of_water_ptr->set_water_glass_material(dielectric_ptr);
	add_object(glass_of_water_ptr);
	*/

	Matte* ground_ptr = new Matte;
	ground_ptr->set_cd(0.75);
	ground_ptr->set_ka(0.25);
	ground_ptr->set_kd(0.65);

	Plane* plane_ptr = new Plane(Point3D(0,-0.01,0), Normal(0,1,0));
	plane_ptr->set_material(ground_ptr);
	add_object(plane_ptr);




}

void
World::build2828(int depth) {
}

