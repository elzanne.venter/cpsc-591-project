// 	Copyright (C) Kevin Suffern 2000-2007.
//	This C++ code is for non-commercial purposes only.
//	This C++ code is licensed under the GNU General Public License Version 2.
//	See the file COPYING.txt for the full license.


#include <ConcavePartTorus.h>
#include "Maths.h"	// for SolveQuartic


// ---------------------------------------------------------------- default constructor

ConcavePartTorus::ConcavePartTorus (void)
	: 	GeometricObject(),
		a(2.0),
		b(1.0),
		bbox(-a - b, a + b, -b, b, -a - b, a + b),
		phi_min(0.0),			// in radians
		phi_max(TWO_PI),		// in radians
		theta_min(0.0),			// in radians measured from top
		theta_max(PI),			// in radians measured from top
		cos_theta_min(1.0),
		cos_theta_max(-1.0)
{}

// ---------------------------------------------------------------- constructor

ConcavePartTorus::ConcavePartTorus (const double _a, const double _b,
		const double azimuth_min,	// in degrees
											const double 	azimuth_max,	// in degrees
											const double 	polar_min,		// in degrees measured from top
											const double 	polar_max)
	: 	GeometricObject(),
		a(_a),
		b(_b),
		bbox(-a - b, a + b, -b, b, -a - b, a + b),
		phi_min(azimuth_min  * PI_ON_180),			// in radians
		phi_max(azimuth_max  * PI_ON_180),		// in radians
		theta_min(polar_min  * PI_ON_180),			// in radians measured from top
		theta_max(polar_max  * PI_ON_180),			// in radians measured from top
		cos_theta_min(cos(theta_min)),
		cos_theta_max(cos(theta_max))
{}																			
																				
// ---------------------------------------------------------------- clone

ConcavePartTorus*
ConcavePartTorus::clone(void) const {
	return (new ConcavePartTorus(*this));
}																				
																				

// ---------------------------------------------------------------- copy constructor																																											

ConcavePartTorus::ConcavePartTorus (const ConcavePartTorus& torus)
	: 	GeometricObject(torus),
		a(torus.a),
		b(torus.b),
		bbox(torus.bbox),
		phi_min(torus.phi_min),			// in radians
		phi_max(torus.phi_max),		// in radians
		theta_min(torus.theta_min),			// in radians measured from top
		theta_max(torus.theta_max),			// in radians measured from top
		cos_theta_min(torus.cos_theta_min),
		cos_theta_max(torus.cos_theta_max)
{}


// ---------------------------------------------------------------- assignment operator	

ConcavePartTorus&
ConcavePartTorus::operator = (ConcavePartTorus& rhs) {
	if (this == &rhs)
		return (*this);

	GeometricObject::operator=(rhs);
				
	a		= rhs.a;
	b 		= rhs.b;
	bbox	= rhs.bbox;	
	phi_min = rhs.phi_min;
	phi_max = rhs.phi_max;
	theta_min = rhs.theta_min;
	theta_max = rhs.theta_max;
	cos_theta_min = rhs.cos_theta_min;
	cos_theta_max = rhs.cos_theta_max;
	
	return (*this);
}


//------------------------------------------------------------------- destructor 	

ConcavePartTorus::~ConcavePartTorus(void) {}


//------------------------------------------------------------------ FindNormal
// Find the normal vector at the specified position
// This works because the torus is defined by a single implicit equation 

Normal
ConcavePartTorus::compute_normal(const Point3D& p) const {
	Normal normal;
	double param_squared = a * a + b * b;

	double x = p.x;
	double y = p.y;
	double z = p.z;
	double sum_squared = x * x + y * y + z * z;
	
	normal.x = 4.0 * x * (sum_squared - param_squared);
	normal.y = 4.0 * y * (sum_squared - param_squared + 2.0 * a * a);
	normal.z = 4.0 * z * (sum_squared - param_squared);
	normal.normalize();
	
	return (normal);	
}


//---------------------------------------------------------------- hit 
	
//***MODIFY FROM HERE
bool 																 
ConcavePartTorus::hit(const Ray& ray, double& tmin, ShadeRec& sr) const {
	if (!bbox.hit(ray))
		return (false);
		
	double x1 = ray.o.x; double y1 = ray.o.y; double z1 = ray.o.z;
	double d1 = ray.d.x; double d2 = ray.d.y; double d3 = ray.d.z;	
	
	double phi;
	double theta;

	double coeffs[5];	// coefficient array for the quartic equation
	double roots[4];	// solution array for the quartic equation
	
	// define the coefficients of the quartic equation
	
	double sum_d_sqrd 	= d1 * d1 + d2 * d2 + d3 * d3;
	double e			= x1 * x1 + y1 * y1 + z1 * z1 - a * a - b * b;
	double f			= x1 * d1 + y1 * d2 + z1 * d3;
	double four_a_sqrd	= 4.0 * a * a;
	
	coeffs[0] = e * e - four_a_sqrd * (b * b - y1 * y1); 	// constant term
	coeffs[1] = 4.0 * f * e + 2.0 * four_a_sqrd * y1 * d2;
	coeffs[2] = 2.0 * sum_d_sqrd * e + 4.0 * f * f + four_a_sqrd * d2 * d2;
	coeffs[3] = 4.0 * sum_d_sqrd * f;
	coeffs[4] = sum_d_sqrd * sum_d_sqrd;  					// coefficient of t^4
	
	// find roots of the quartic equation
	
	int num_real_roots = SolveQuartic(coeffs, roots);
	
	bool	intersected = false;
	double 	t 		 	= kHugeValue;
	
	if (num_real_roots == 0)  // ray misses the torus
		return(false);
	
	// find the smallest root greater than kEpsilon, if any
	// the roots array is not sorted
			
	for (int j = 0; j < num_real_roots; j++)  
		if (roots[j] > kEpsilon) {

			double temp = roots[j];
			Vector3D torus_hit = ray.o + temp * ray.d; //hit for the whole torus


			Vector3D hit_xz_projection = Vector3D (torus_hit.x, 0.0, torus_hit.z); //projection to the xz plane
			hit_xz_projection.normalize();

			phi = acos(hit_xz_projection * Vector3D(1,0,0));
			theta = acos((pow(torus_hit.length(),2) - pow(a,2) - pow(b,2))/(2*a*b));

			if (hit_xz_projection.z>0){
				phi = TWO_PI - phi;
			}

			if (torus_hit.y < 0){
				theta = TWO_PI - theta;
			}

			if ((roots[j] < t) && ((phi > phi_min) && (phi<phi_max)) &&  ((theta > theta_min) && (theta < theta_max))   )
				t = roots[j];
				intersected = true;
		}
		
	if(!intersected)
		return (false);
		
	tmin 			 	= t;
	sr.local_hit_point 	= ray.o + t * ray.d;
	sr.normal 		 	= -compute_normal(sr.local_hit_point);
	
	return (true);
}

//***MODIFY FROM HERE
bool
ConcavePartTorus::shadow_hit(const Ray& ray, double& tmin) const {
	if (!bbox.hit(ray))
		return (false);

	double x1 = ray.o.x; double y1 = ray.o.y; double z1 = ray.o.z;
	double d1 = ray.d.x; double d2 = ray.d.y; double d3 = ray.d.z;

	double phi;
	double theta;

	double coeffs[5];	// coefficient array for the quartic equation
	double roots[4];	// solution array for the quartic equation

	// define the coefficients of the quartic equation

	double sum_d_sqrd 	= d1 * d1 + d2 * d2 + d3 * d3;
	double e			= x1 * x1 + y1 * y1 + z1 * z1 - a * a - b * b;
	double f			= x1 * d1 + y1 * d2 + z1 * d3;
	double four_a_sqrd	= 4.0 * a * a;

	coeffs[0] = e * e - four_a_sqrd * (b * b - y1 * y1); 	// constant term
	coeffs[1] = 4.0 * f * e + 2.0 * four_a_sqrd * y1 * d2;
	coeffs[2] = 2.0 * sum_d_sqrd * e + 4.0 * f * f + four_a_sqrd * d2 * d2;
	coeffs[3] = 4.0 * sum_d_sqrd * f;
	coeffs[4] = sum_d_sqrd * sum_d_sqrd;  					// coefficient of t^4

	// find roots of the quartic equation

	int num_real_roots = SolveQuartic(coeffs, roots);

	bool	intersected = false;
	double 	t 		 	= kHugeValue;

	if (num_real_roots == 0)  // ray misses the torus
		return(false);

	// find the smallest root greater than kEpsilon, if any
	// the roots array is not sorted

	for (int j = 0; j < num_real_roots; j++)
		if (roots[j] > kEpsilon) {
			double temp_t = roots[j];
			Vector3D hit = ray.o + temp_t * ray.d;
			Vector3D hit_xz_plane = Vector3D (hit.x, 0.0, hit.z);
			hit_xz_plane.normalize();
			phi = acos(hit_xz_plane * Vector3D(1,0,0));

			if (hit_xz_plane.z>0){
				phi = TWO_PI - phi;
			}

			theta = acos((pow(hit.length(),2) - pow(a,2) - pow(b,2))/(2*a*b));

			if (hit.y < 0){
				theta = TWO_PI - theta;
			}

			if ((roots[j] < t) && ((theta > theta_min) && (theta < theta_max)) &&  ((phi > phi_min) && (phi<phi_max)) )
				t = roots[j];
				intersected = true;
		}

	if(!intersected)
		return (false);

	tmin 			 	= t;

	return (true);
}

